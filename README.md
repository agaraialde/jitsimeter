# Jitsimeter
¿Qué instancia de Jitsi me conviene usar?

Genera un sitio web para comparar instancias la  velocidad y seguridad de diferentes instancias de Jitsi.

Versión pública: https://ladatano.partidopirata.com.ar/jitsimeter/

Licencia: GPLv3

![Captura de pantalla de Jitsimeter](screenshot.png)

## Instrucciones

Jitsi es una sistema de videoconferencias de software libre que se puede instalar en muchos servidores, por lo que es normal que, aunque se vean iguales, los diferentes servidores ofrezcan diferentes prestaciones.

Jitsimeter permite a cada usuario que entra pueda evaluar más de 200 instancias de Jitsi para saber cuál es la más rápida y la más segura para utilizar. El test tarda 4 minutos y consume datos, deshabilitar otras descargas durante la prueba. Luego de la prueba elegir la instancia con mejor descarga y velocidad, se puede acceder directamente desde el enlace, no requiere registración.



### Pruebas
Lo que esta herramienta intenta hacer es diferenciar las cualidades de infraestructura de los servidores, tanto si protegen tu privacidad como si te van a brindar una buena experiencia de llamadas porque son servidores rápidos.

- Velocidad: Realiza una solicitud GET a un archivo .wav que está presente en todas las instancias de Jitsi, y mide el tiempo que tarda en cargar, por lo que, sabiendo el tamaño del archivo, podemos estimar la velocidad de la conexión. Esta prueba está basada en [jqspeedtest](https://github.com/skycube/jqspeedtest).
- Respuesta: Realiza una solicitud HEAD y mide el tiempo que tarda en responder. Esta prueba está basada en [jqspeedtest](https://github.com/skycube/jqspeedtest).
- Privacidad STUN: Para encontrar a otros usuarios de jitsi hay que usar lo que se llama un servidor STUN, y muchas instancias de jitsi usan el servidor que provee google, que es gratuito y abierto, pero te deja expuesto a que google sepa que estás iniciando una conversación, por lo que se prefieren otros servidores. Esta prueba está basada en [jitsi-list-generator](https://git.jugendhacker.de/j.r/jitsi-list-generator/).
- Privacidad Host: Si el sitio está alojado por google, amazon, cloudflare, o microsoft, se considera que estas empresas abusan de la información que reciben para correlacionarla con otra que tienen, y que incluso se la podrían entregar a los servicios de inteligencia, por lo tanto no se recomienda utilizarlos como proveedor de alojamiento web. Esta prueba está basada en [jitsi-list-generator](https://git.jugendhacker.de/j.r/jitsi-list-generator/).

Nota: Todas las instancias de la lista de instancias aparecen siempre en el sitio, pero algunas vienen con "available: false", esto significa que jitsi-list-generator no se pudo conectar, aunque esta información no aparece en el listado e igual se realizan las pruebas. Hay muchos servidores que rechazan la conexión, porque no están disponibles o porque bloquean las solicitudes entre sitios, aunque esto se hace con el objetivo de limitar los ataques de tipo CORS, perjudica el funcionamiento de Jitsimeter. Los servidores que fallan aparecen con -1 en los testeos.

## Armar tu propia instancia de este sitio
Este sitio tiene licencia GPLv3, lo que significa que puedes copiarlo y modificarlo solamente manteniendo la misma licencia y atribuyendo la autoría del código.

Parar armar tu propia instancia sólo es necesario que tengas git para descargar los archivos, y acceso a un servidor web para publicarlos. Si quieres hacer tu propia lista también necesitarás python.

## Pasos
- Clonar el proyecto: ```git clone https://0xacab.org/faras/jitsimeter/```
- Clonar jitsi-list-generator: ```git clone https://git.jugendhacker.de/j.r/jitsi-list-generator/```
- Seguir las instrucciones de instalación de jitsi-list-generator aquí: https://git.jugendhacker.de/j.r/jitsi-list-generator/src/branch/feature-json-output#user-content-install
- Si quieres agregar tu propia instancia, debes editar el archivo instances.txt, ej: ```echo '"miinstancia.com",' > instances.txt```.
- Generar lista: ```python3 main.py ../jitsimeter/instances.txt json > ../jitsimeter/instances.json```

Listo, ya tienes tu versión propia con lista actualizada, sólo te queda publicarla.

## Colaboración

Si gustas contar tu experiencia, proponer nuevas instancias o contactarte puedes utilizar los issues de 0xacab.org aquí: https://0xacab.org/faras/jitsimeter/issues o escrbir a faras@partidopirata.com.ar

La lista original de instancias es de [favstarmafia](https://fediverse.blog/~/DonsBlog/jitsi-server-liste-text).
Gracias a las piratas de la barca de infra, a las compas de autodefensa, latam y el club de software libre.

### Traducciones
Para traducciones estamos usando [este issue](https://0xacab.org/faras/jitsimeter/issues/1).

Gracias a les traductores y revisores:
- Inglés, italiano, francés, catalán: Ona (+apertium).
- Alemán: Favstarmafia, Sofía y heroínas anónimas.
