const instancesURL = "./instances.json";

let instanceCounts = {
  total: 0,
  tested: 0,
  failed: 0
}

const available_locales = ["es","en","fr","de","ca","it"];

var current_locale = navigator.languages.map(function(language) {
  return language.split('-')[0];
}).find(function(locale) {
  return available_locales.includes(locale);
});

if (current_locale == undefined) current_locale = available_locales[0];

console.log("Locale:",current_locale);


$(document).ready(function() {
    $("#notice-progress,#notice-finish").hide();
    $('#instancesTable').DataTable( {
        "ajax": instancesURL,
        info: false,
        order: 3,
        "paging":   false,
        "columns": [
            { "data": "domain", render: function(value) { return "<a href='https://"+value+"'>"+value+"</a>"; } },
            { "data": "download", type: "num", defaultContent: "-", className: "dt-body-right", render: function(value) { return (value === false) ? "&#10006;" : value; }},
            { "data": "rtt", type: "num-fmt", defaultContent: "-", className: "dt-body-right", render: function(value) { return (value === false) ? "&#10006;" : value; } },
            { "data": "countryCode", className: "dt-body-center" },
            { "data": "bad", className: "dt-body-center", render: function(value) { return (value === true) ? "&#10006;" : "&#10004;"; } },
            { "data": "hosterBad", className: "dt-body-center", render: function(value) { return (value === true) ? "&#10006;" : "&#10004;"; } },
            { "data": "hoster" },
            { "data": "tested", className: "dt-body-center", defaultContent: "-", render: function(value) { return (value === true) ? "&#10004;" : (value === window.undefined) ? "-" : (value === "progress") ? "⟳" : "&#10006;"; } }
        ],
        pageLength: 300,
        initComplete: function() {
          var info = this.DataTable().page.info();
          instanceCounts.total = info.recordsTotal;
          updateProgress();
          $('#tableInfo').html(info.recordsTotal);
        }
    } );

    $("#start").click(startTest)

     //Multilang
     available_locales.forEach(function(locale) {
       if (locale == current_locale) return;
       document.querySelectorAll('html [lang='+locale+']').forEach(function(el) {
         el.parentNode.removeChild(el);
       });
     });

} );


async function startTest() {
  let table = $('#instancesTable').DataTable();
  let data = table.data();
  $("#notice-stop").hide();
  $("#notice-progress").show()

  $.each(data, function(i,instance) {
    try {
      instance.table = table;
      $cell = $("table td:contains('"+instance.domain+"')");
      let index = instance.table.cell($cell).index();

      instance.row = instance.table.row(index.row);
      instance.downloadCell = instance.table.cell({row: index.row, column: 1});
      instance.rttCell = instance.table.cell({row: index.row, column: 2});
      instance.testedCell = instance.table.cell({row: index.row, column: 7});

      if (instance.available == false) {
        failed(instance);
        return true;
      }

      setTimeout(function() {
          jitsi_test($('#instancesTable'),i, instance);
      }, 1000*i)
    }
    catch(e) {
      console.error("Error jitsi_test:",e,i,instance);
    }
  })
}

function updateProgress() {
  $('#instancesTable').DataTable().order(1).draw();

  $("#progress").attr("max",instanceCounts.total);
  $("#progress").attr("value",(instanceCounts.failed+instanceCounts.tested));

  if (instanceCounts.total == (instanceCounts.failed+instanceCounts.tested)) {
    $("#notice-progress").hide();
    $("#notice-finish").show()
  }
}

function jitsi_test(table,rowNumber, instance) {
  instance.testedCell.data("progress");
  updateProgress();

  let testURL = "https://"+instance.domain+"/sounds/outgoingStart.wav";

  TestResponse(testURL, function(response) {
    // console.log("jitsi_rtt_test success", response)
    if (response != -1) {
      instance.rttCell.data(response);

      TestDownload(testURL, function(kbps) {
        // console.log("jitsi_download_test success", kbps)
        instance.downloadCell.data(kbps);
        instance.testedCell.data(true);
        instanceCounts.tested+=1;
        updateProgress();
      } )
    }
    else {
      failed(instance);
    }
  } )


}

function failed(instance) {
  instanceCounts.failed+=1;
  instance.rttCell.data(false);
  instance.testedCell.data(false);
  updateProgress();
}

//** Test the download speed
// based in https://github.com/skycube/jqspeedtest
function TestDownload(url,callback){
  var sendDate = (new Date()).getTime();
  // console.log("TestDownload", url);
  $.ajax({
    type: "GET",
    url: url,
    timeout: 60000,
    cache: false,
    success: function(){
      var receiveDate = (new Date()).getTime();
      var duration = (receiveDate - sendDate) / 1000;
      var bitsLoaded = 206.34 * 1024 * 8;
      var speedBps = (bitsLoaded / duration).toFixed(2);
      var speedKbps = (speedBps / 1024).toFixed(0);
      var speedMbps = (speedKbps / 1024).toFixed(2);
      var speedGbps = (speedMbps / 1024).toFixed(2);

      callback(speedKbps);

    },
    error: function() { callback( -1) }
  });
}


//** Test Response time
// based in https://github.com/skycube/jqspeedtest
function TestResponse(url,callback){
  var sendDate = (new Date()).getTime();
  // console.log("TestResponse",sendDate)
  $.ajax({
    type: "HEAD",
    url: url,
    timeout: 5000,
    cache: false,
    success: function(a,b,c){
      // console.log("tr suc",a,b,c)
      var receiveDate = (new Date()).getTime();
      var response = receiveDate - sendDate;

      callback(response);
    },
    error: function() { callback( -1) }
  });
}
